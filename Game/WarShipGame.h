#pragma once
#define _USE_MATH_DEFINES
#include "Game.h"
#include "d3dx11effect.h"
#include <math.h>
#include "Logger.h"
#include "Vertex.h"
#include <vector>
#include "Mesh.h"
#include "SimpleEffect.h"
#include "Model.h"

class WarShipGame : public Game
{
private:

	ID3D11Buffer* vertexBuffer;
	ID3D11Buffer* indexBuffer;
	ID3DX11Effect* effect;

	ID3D11InputLayout* inputLayout;

	typedef DirectX::XMFLOAT4X4 MATRIX;

	MATRIX world;
	MATRIX view;
	MATRIX projection;

	SimpleEffect *fx;
	std::vector<Model<VertexPositionColor>*> *models;

public:
	WarShipGame(HINSTANCE hinstance);
	~WarShipGame();

	bool Initialization() override;
	void Update(const GameTime&) override;
	void Draw(const GameTime&) override;


	void OnResize() override;
private:
	void BuildGeometryBuffers();
	void BuildVertexLayout();
	void SetModels();
};

