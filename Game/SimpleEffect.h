#pragma once
#include "Effects.h"

class SimpleEffect : public Effect
{
private:
	ID3DX11EffectTechnique* technique;
	ID3DX11EffectMatrixVariable* worldViewProjMatrix;
public:
	SimpleEffect(ID3D11Device* device, const std::string& filename) : Effect(device, filename)
	{
		technique = fx->GetTechniqueByName("ColorTech");
		worldViewProjMatrix = fx->GetVariableByName("gWorldViewProj")->AsMatrix();;
	};

	void SetWorldViewProjectionMatrix(CXMMATRIX m)
	{
		worldViewProjMatrix->SetMatrix(reinterpret_cast<const float*>(&m));
	};

	inline ID3DX11EffectTechnique*  GetTechnique() const
	{
		return technique;
	}
};

