//***************************************************************************************
// LightHelper.fx Frank Luna (C) 2011 Wszelkie prawa zastrzeżone.
//
// Struktury i funkcje do obliczania oświetlenia.
//***************************************************************************************

struct DirectionalLight
{
	float4 Ambient;
	float4 Diffuse;
	float4 Specular;
	float3 Direction;
	float pad;
};

struct PointLight
{ 
	float4 Ambient;
	float4 Diffuse;
	float4 Specular;

	float3 Position;
	float Range;

	float3 Att;
	float pad;
};

struct SpotLight
{
	float4 Ambient;
	float4 Diffuse;
	float4 Specular;

	float3 Position;
	float Range;

	float3 Direction;
	float Spot;

	float3 Att;
	float pad;
};

struct Material
{
	float4 Ambient;
	float4 Diffuse;
	float4 Specular; // w = SpecPower
	float4 Reflect;
};

//---------------------------------------------------------------------------------------
// Oblicza składniki światła otoczenia, rozproszonego i odbitego we wzorze na oświetlenie
// na podstawie światła kierunkowego. Składniki zwracamy oddzielnie, ponieważ później
// będą pojedynczo modyfikowane.
//---------------------------------------------------------------------------------------
void ComputeDirectionalLight(Material mat, DirectionalLight L, 
                             float3 normal, float3 toEye,
					         out float4 ambient,
						     out float4 diffuse,
						     out float4 spec)
{
	// Inicjalizuj argumenty wyjściowe.
	ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	spec    = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// Wektor światła jest zwrócony w kierunku przeciwnym do kierunku padania promieni.
	float3 lightVec = -L.Direction;

	// Dodaj składnik światła otoczenia.
	ambient = mat.Ambient * L.Ambient;	

	// Dodaj składniki światła rozproszonego i odbitego pod warunkiem, że pada ono na powierzchnię.
	
	float diffuseFactor = dot(lightVec, normal);

	// Spłaszcz, aby uniknąć dynamicznego rozgałęziania.
	[flatten]
	if( diffuseFactor > 0.0f )
	{
		float3 v         = reflect(-lightVec, normal);
		float specFactor = pow(max(dot(v, toEye), 0.0f), mat.Specular.w);
					
		diffuse = diffuseFactor * mat.Diffuse * L.Diffuse;
		spec    = specFactor * mat.Specular * L.Specular;
	}
}

//---------------------------------------------------------------------------------------
// Oblicza składniki światła otoczenia, rozproszonego i odbitego we wzorze na oświetlenie
// na podstawie światła punktowego. Składniki zwracamy oddzielnie, ponieważ później
// będą pojedynczo modyfikowane.
//---------------------------------------------------------------------------------------
void ComputePointLight(Material mat, PointLight L, float3 pos, float3 normal, float3 toEye,
				   out float4 ambient, out float4 diffuse, out float4 spec)
{
	// Inicjalizuj argumenty wyjściowe.
	ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	spec    = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// Wektor z powierzchni do źródła.
	float3 lightVec = L.Position - pos;
		
	// Odległość między powierzchnią a źródłem.
	float d = length(lightVec);
	
	// Test zasięgu.
	if( d > L.Range )
		return;
		
	// Normalizuj wektor światła.
	lightVec /= d; 
	
	// Składnik światła otoczenia.
	ambient = mat.Ambient * L.Ambient;	

	// Dodaj składniki światła rozproszonego i odbitego pod warunkiem, że pada ono na powierzchnię.

	float diffuseFactor = dot(lightVec, normal);

	// Spłaszcz, aby uniknąć dynamicznego rozgałęziania.
	[flatten]
	if( diffuseFactor > 0.0f )
	{
		float3 v         = reflect(-lightVec, normal);
		float specFactor = pow(max(dot(v, toEye), 0.0f), mat.Specular.w);
					
		diffuse = diffuseFactor * mat.Diffuse * L.Diffuse;
		spec    = specFactor * mat.Specular * L.Specular;
	}

	// Wygaszanie
	float att = 1.0f / dot(L.Att, float3(1.0f, d, d*d));

	diffuse *= att;
	spec    *= att;
}

//---------------------------------------------------------------------------------------
// Oblicza składniki światła otoczenia, rozproszonego i odbitego we wzorze na oświetlenie
// na podstawie światła reflektorowego. Składniki zwracamy oddzielnie, ponieważ później
// będą pojedynczo modyfikowane.
//---------------------------------------------------------------------------------------
void ComputeSpotLight(Material mat, SpotLight L, float3 pos, float3 normal, float3 toEye,
				  out float4 ambient, out float4 diffuse, out float4 spec)
{
	// Inicjalizuj argumenty wyjściowe.
	ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	spec    = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// Wektor z powierzchni do źródła.
	float3 lightVec = L.Position - pos;
		
	// Odległość między powierzchnią a źródłem.
	float d = length(lightVec);
	
	// Test zasięgu.
	if( d > L.Range )
		return;
		
	// Normalizuj wektor światła.
	lightVec /= d; 
	
	// Składnik światła otoczenia.
	ambient = mat.Ambient * L.Ambient;	

	// Dodaj składniki światła rozproszonego i odbitego pod warunkiem, że pada ono na powierzchnię.

	float diffuseFactor = dot(lightVec, normal);

	// Spłaszcz, aby uniknąć dynamicznego rozgałęziania.
	[flatten]
	if( diffuseFactor > 0.0f )
	{
		float3 v         = reflect(-lightVec, normal);
		float specFactor = pow(max(dot(v, toEye), 0.0f), mat.Specular.w);
					
		diffuse = diffuseFactor * mat.Diffuse * L.Diffuse;
		spec    = specFactor * mat.Specular * L.Specular;
	}
	
	// Skalowanie za pomocą współczynnika światła reflektorowego i wygaszanie
	float spot = pow(max(dot(-lightVec, L.Direction), 0.0f), L.Spot);

	// Skalowanie za pomocą współczynnika światła reflektorowego i wygaszanie
	float att = spot / dot(L.Att, float3(1.0f, d, d*d));

	ambient *= spot;
	diffuse *= att;
	spec    *= att;
}

 
 