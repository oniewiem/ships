//=============================================================================
// Basic.fx Frank Luna (C) 2011 Wszelkie prawa zastrzeżone.
//
// Efekt bazowy. Aktualnie obsługuje przekształcenia, oświetlenie 
// oraz tekstury.
//=============================================================================

#include "LightHelper.fx"
 
cbuffer cbPerFrame
{
	DirectionalLight gDirLights[3];
	float3 gEyePosW;

	float  gFogStart;
	float  gFogRange;
	float4 gFogColor;
};

cbuffer cbPerObject
{
	float4x4 gWorld;
	float4x4 gWorldInvTranspose;
	float4x4 gWorldViewProj;
	float4x4 gTexTransform;
	Material gMaterial;
}; 

// Danych nienumerycznych nie można zapisywać w obiekcie cbuffer
Texture2D gDiffuseMap;

SamplerState samAnisotropic
{
	Filter = ANISOTROPIC;
	MaxAnisotropy = 4;

	AddressU = WRAP;
	AddressV = WRAP;
};

struct VertexIn
{
	float3 PosL    : POSITION;
	float3 NormalL : NORMAL;
};

struct VertexOut
{
	float4 PosH    : SV_POSITION;
    float3 PosW    : POSITION;
    float3 NormalW : NORMAL;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout;
	
	// Przekształcenie do przestrzeni świata.
	vout.PosW    = mul(float4(vin.PosL, 1.0f), gWorld).xyz;
	vout.NormalW = mul(vin.NormalL, (float3x3)gWorldInvTranspose);
		
	// Przekształcenie do jednorodnej przestrzeni obcinania.
	vout.PosH = mul(float4(vin.PosL, 1.0f), gWorldViewProj);
	
	return vout;
}
 
float4 PS(VertexOut pin, uniform int gLightCount) : SV_Target
{
	// Normalizuj ponownie normalną (mogła ulec denormalizacji w procesie interpolacji).
    pin.NormalW = normalize(pin.NormalW);

	// Wektor toEye jest używany przy oświetlaniu.
	float3 toEye = gEyePosW - pin.PosW;

	// Przechowaj odległość tego punktu powierzchni od oka.
	float distToEye = length(toEye); 

	// Normalizuj.
	toEye /= distToEye;
	
	//
	// Oświetlenie.
	//


	// Rozpocznij sumowanie od zera.
	float4 ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 spec    = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// Sumuj udział światła z każdego źródła. 
	[unroll]
	for(int i = 0; i < gLightCount; ++i)
	{
		float4 A, D, S;
		ComputeDirectionalLight(gMaterial, gDirLights[i], pin.NormalW, toEye, 
			A, D, S);

		ambient += A;
		diffuse += D;
		spec    += S;
	}

	float4 litColor = ambient + diffuse + spec;

	// Pobierz wartość alfa z koloru materiału rozpraszającego i tekstury.
	litColor.a = gMaterial.Diffuse.a;

    return litColor;
}

technique11 Light1
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_5_0, VS() ) );
		SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_5_0, PS(1) ) );
    }
}

technique11 Light2
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_5_0, VS() ) );
		SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_5_0, PS(2) ) );
    }
}

technique11 Light3
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_5_0, VS() ) );
		SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_5_0, PS(3) ) );
    }
}
