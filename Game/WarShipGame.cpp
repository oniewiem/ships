#include "WarShipGame.h"
#include <fstream>
#include "Mesh.h"
#include "SimpleEffect.h"


using namespace DirectX;

WarShipGame::WarShipGame(HINSTANCE hinstance) : Game(hinstance), 
vertexBuffer(0), indexBuffer(0), effect(0), inputLayout(0)
{
	XMMATRIX identity = XMMatrixIdentity();
	
	XMStoreFloat4x4(&world, identity);
	XMStoreFloat4x4(&view, identity);
	XMStoreFloat4x4(&projection, identity);
	title = "Warship game";
	try {
		Mesh<int>m = Mesh<int>();
	}catch(std::exception ex)
	{
		LOG_ERROR(1, "Mesh construct error '%s'", ex.what());
	}
}

WarShipGame::~WarShipGame()
{
	Memory::SaveRelease(vertexBuffer);
	Memory::SaveRelease(indexBuffer);
	Memory::SaveRelease(effect);
	Memory::SaveRelease(inputLayout);
	Memory::SaveDelete(fx);
}

bool WarShipGame::Initialization()
{
	if(!Game::Initialization())
	{
		LOG_ERROR(1, "Initialization game faild");
		return false;
	}
	LOG_TRACE(1, "Init Game");
	fx = new SimpleEffect(device, "FX/color.fxo");

	SetModels();
	

	BuildVertexLayout();
	
	return true;
}

void WarShipGame::Update(const GameTime& gameTime)
{
	XMVECTOR camPos = XMVectorSet(sin(gameTime.GetTotalGameTime().GetTotalSeconds() * 0.25f) * 25, 5 + abs( sin(gameTime.GetTotalGameTime().GetTotalSeconds()* 0.1f) ) * 20, cos(gameTime.GetTotalGameTime().GetTotalSeconds()* 0.25f) * 50, 1.0);

	XMVECTOR camTarget = XMVectorSet(0, 0, 0, 1);
	XMVECTOR camUp = XMVectorSet(0, 1, 0, 1);

	XMMATRIX v = XMMatrixLookAtLH(camPos, camTarget, camUp);
	
	XMStoreFloat4x4(&view, v);

	Model<VertexPositionColor> *m = models->at(1);
	m->SetRotation(XMVectorSet(0.0f, 0.001f * gameTime.GetTotalGameTime().GetTotalMilliseconds(), 0.0f, 1.0f));
	m->SetPosition(XMVectorSet(sin(gameTime.GetTotalGameTime().GetTotalSeconds() * 0.25f) * 3, 1.0f, cos(gameTime.GetTotalGameTime().GetTotalSeconds() * 0.25f) * 2, 1.0f));

	m = models->at(2);
	m->SetScale(XMVectorSet(10.0f + abs(sin(gameTime.GetTotalGameTime().GetTotalMilliseconds() * 0.00025f) * 25), 1.0f, 10.0f + abs(cos(gameTime.GetTotalGameTime().GetTotalMilliseconds() * 0.00025f) * 25), 1.0f));
}

void WarShipGame::Draw(const GameTime& gameTime)
{
	context->ClearRenderTargetView(renderTargetView, DirectX::Colors::Black);
	context->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0.0f);
	context->IASetInputLayout(inputLayout);

	UINT stride = sizeof(VertexPositionColor), vertexOffset = 0, indexOffset = 0;
	for (std::vector<Model<VertexPositionColor>*>::iterator it = models->begin(); it != models->end(); ++it)
	{
		context->IASetPrimitiveTopology((*it)->Topology);
		UINT offset = 0;
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
		XMMATRIX w = (*it)->GetMatrix();
		XMMATRIX v = XMLoadFloat4x4(&view);
		XMMATRIX p = XMLoadFloat4x4(&projection);
		XMMATRIX worldViewProjection = w*v*p;

		fx->SetWorldViewProjectionMatrix(worldViewProjection);
		D3DX11_TECHNIQUE_DESC techDesc;

		fx->GetTechnique()->GetDesc(&techDesc);
		for (UINT p = 0; p < techDesc.Passes; ++p)
		{
			fx->GetTechnique()->GetPassByIndex(p)->Apply(0, context);
			context->DrawIndexed((*it)->GetMesh()->GetIndexCount(), indexOffset, vertexOffset);
		}
		indexOffset += ((*it)->GetMesh()->GetIndexCount());
		vertexOffset += ((*it)->GetMesh()->GetVertexCount());
	}

	//XMStoreFloat4x4(&world, XMMatrixScaling(10.0f, 10.0f, 10.0f));
	
//	XMMATRIX w = cubeModel->GetMatrix();
	
	HR(swapChain->Present(0, 0));
}

void WarShipGame::OnResize()
{
	Game::OnResize();

	XMMATRIX P = XMMatrixPerspectiveFovLH(0.25 * M_PI, width / height, 1.0f, 1000.0f);
	XMStoreFloat4x4(&projection, P);
}

void WarShipGame::SetModels()
{
	models = new std::vector<Model<VertexPositionColor>*>();
	Model<VertexPositionColor>* axisModel = new Model<VertexPositionColor>(new Axis<VertexPositionColor>());
	Model<VertexPositionColor>* cubeModel = new Model<VertexPositionColor>(new Cube<VertexPositionColor>());
	Model<VertexPositionColor>* planeModel = new Model<VertexPositionColor>(new Cube<VertexPositionColor>());

	axisModel->SetScale(XMVectorSet(100.0f, 100.0f, 100.0f, 1.0f));
	axisModel->SetPosition(XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f));
	axisModel->SetRotation(XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f));
	axisModel->Topology = D3D10_PRIMITIVE_TOPOLOGY_LINELIST;

	cubeModel->SetScale(XMVectorSet(2.0f, 2.0f, 2.0f, 1.0f));
	cubeModel->SetPosition(XMVectorSet(0.0f, 0.75f, 0.0f, 1.0f));
	cubeModel->SetRotation(XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f));
	cubeModel->Topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	planeModel->SetScale(XMVectorSet(20.0f, 1.0f, 20.0f, 1.0f));
	planeModel->SetPosition(XMVectorSet(0.5f, -0.5f, 0.5f, 1.0f));
	planeModel->SetRotation(XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f));
	planeModel->Topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	models->push_back(axisModel);
	models->push_back(cubeModel);
	models->push_back(planeModel);

	BuildGeometryBuffers();
}


void WarShipGame::BuildGeometryBuffers()
{
	std::vector<VertexPositionColor> allVertex = std::vector<VertexPositionColor>();
	std::vector<UINT> allIndex = std::vector<UINT>();

	for (std::vector<Model<VertexPositionColor>*>::iterator it = models->begin(); it != models->end(); ++it)
	{
		allVertex.insert(allVertex.end(), (*it)->GetMesh()->GetVerticesArray(), (*it)->GetMesh()->GetVerticesArray() + (*it)->GetMesh()->GetVertexCount());
		allIndex.insert(allIndex.end(), (*it)->GetMesh()->GetIndicesArray(), (*it)->GetMesh()->GetIndicesArray() + (*it)->GetMesh()->GetIndexCount());
	}
	
	D3D11_BUFFER_DESC vbd;
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.ByteWidth = sizeof(VertexPositionColor) * allVertex.size();
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA vinitData;
	vinitData.pSysMem = allVertex.data();

	HR(device->CreateBuffer(&vbd, &vinitData, &vertexBuffer));

	D3D11_BUFFER_DESC ibd;
	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.ByteWidth = sizeof(UINT) * allIndex.size();
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.MiscFlags = 0;
	ibd.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA iinitData;
	iinitData.pSysMem = allIndex.data();
	HR(device->CreateBuffer(&ibd, &iinitData, &indexBuffer));
}

void WarShipGame::BuildVertexLayout()
{
	D3DX11_PASS_DESC passDesc;
	fx->GetTechnique()->GetPassByIndex(0)->GetDesc(&passDesc);
	HR(device->CreateInputLayout(VertexPositionColorDesc, 2, passDesc.pIAInputSignature,
		passDesc.IAInputSignatureSize, &inputLayout));

}
