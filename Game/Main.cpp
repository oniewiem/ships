#include "WarShipGame.h"


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	WarShipGame testApp(hInstance);

	if (!testApp.Initialization())
		return 1;

	return testApp.Run();
}
