#include "Effects.h"

#pragma region Effect
Effect::Effect(ID3D11Device* device, const std::string& filename) : fx(nullptr)
{
	std::ifstream fin(filename, std::ios::binary);
	fin.seekg(0, std::ios_base::end);
	int size = static_cast<int>(fin.tellg());
	fin.seekg(0, std::ios_base::beg);
	std::vector<char> compiledShader(size);

	fin.read(&compiledShader[0], size);
	fin.close();

	HR(D3DX11CreateEffectFromMemory(&compiledShader[0], size,
		0, device, &fx));
}

Effect::~Effect()
{
	Memory::SaveRelease(fx);
}
#pragma endregion

#pragma region BassicEffect
BasicEffect::BasicEffect (ID3D11Device *device,const std::string &filename) : Effect(device, filename)
{
	Light1Tech			= fx->GetTechniqueByName("Light1");
	WorldViewProj		= fx->GetVariableByName("gWorldViewProj")->AsMatrix();
	World				= fx->GetVariableByName("gWorld")->AsMatrix();
	WorldInvTranspose	= fx->GetVariableByName("gWorldInvTranspose")->AsMatrix();
	EyePosW				= fx->GetVariableByName("gEyePosW")->AsVector();
	DirLight			= fx->GetVariableByName("gDirLights");
	Mat					= fx->GetVariableByName("gMaterial");
}
#pragma endregion 
//#pragma region Effects
//void Effects::AddEffect(Effect& effect)
//{
//	effects.push_back(&effect);
//}
//
//void Effects::RemoveAll()
//{
//	for (auto effect : effects)
//	{
//		Memory::SaveDelete(effect);
//	}
//}
//#pragma endregion 
