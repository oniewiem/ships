#pragma once

#include "d3dx11effect.h"
#include <d3d11.h>
#include <D3DX11.h>
#include <DirectXColors.h>
#include "Logger.h"

#pragma comment(lib, "d3d11.lib")
#if defined(DEBUG) | defined(_DEBUG)
#pragma comment(lib, "d3dx11effectD.lib")
#pragma comment(lib, "d3dx11d.lib")
#pragma comment(lib, "d3dcompiler.lib")
#else
#pragma comment(lib, "d3dx11effect.lib")
#pragma comment(lib, "d3dx11.lib")
#endif


#if defined(DEBUG) | defined(_DEBUG)
#ifndef HR
#define HR(x)												   \
	{                                                          \
		HRESULT hr = (x);                                      \
		if(FAILED(hr))                                         \
		{                                                      \
			LOG_ERROR(0, #x);								   \
		}                                                      \
	}
#endif

#else
#ifndef HR
#define HR(x) (x)
#endif
#endif 


namespace Memory
{
	template<class T> void SaveDelete(T& t)
	{
		if (t)
		{
			delete t;
			t = nullptr;
		}
	}

	template<class T> void SaveDeleteArray(T& t)
	{
		if (t)
		{
			delete[] t;
			t = nullptr;
		}
	}

	template<class T> void SaveRelease(T& t)
	{
		if(t)
		{
			t->Release();
			t = nullptr;
		}
	}
	
}