#pragma once
#include "StringUtil.h"
#include <Windows.h>

#ifdef _DEBUG

#define LOG_ERROR(logLevel, ...)    Logger::GetInstance()->Log(__FILE__, __LINE__, __func__, "ERROR", logLevel, __VA_ARGS__)
#define LOG_WARNING(logLevel, ...)  Logger::GetInstance()->Log(__FILE__, __LINE__, __func__,"WARNING", logLevel,  __VA_ARGS__)
#define LOG_MESSAGE(logLevel, ...)  Logger::GetInstance()->Log(__FILE__, __LINE__, __func__, "MESSAGE",logLevel, __VA_ARGS__)
#define LOG_TRACE(logLevel, ...)    Logger::GetInstance()->Log(__FILE__, __LINE__, __func__, "TRACE",logLevel, __VA_ARGS__)
#else
#define LOG_ERROR(logLevel, ...) do{}while(false)
#define LOG_WARNING(logLevel, ...) do{}while(false)
#define LOG_MESSAGE(logLevel, ...) do{}while(false)
#define LOG_TRACE(logLevel, ...) do{}while(false)  
#endif


class Logger
{

public:
	static Logger* GetInstance();
	void Log(const char* file, int line, const char* functionName, const char* kind, int logLevel, const char*, ...);

private:
	std::string ComposeLogToString(const char* file, int line, const char* functionName, const char* kind, int logLevel, const char* text);

};

