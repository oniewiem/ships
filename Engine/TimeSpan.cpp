#include "TimeSpan.h"

const float TimeSpan::SecondPerMillis = 1.0f / MillisPerSecond;
const float TimeSpan::MinutePerMillis = 1.0f / MillisPerMinute;
const float TimeSpan::HourPerMillis = 1.0f / MillisPerHour;
const float TimeSpan::DayPerMillis = 1.0f / MillisPerDay;


TimeSpan::TimeSpan(unsigned milliseconds)
{
	this->milliseconds = milliseconds;
}

TimeSpan TimeSpan::Zero()
{
	return TimeSpan(0);
}

TimeSpan TimeSpan::FromSeconds(double seconds)
{
	return TimeSpan(seconds * MillisPerSecond);
}

TimeSpan TimeSpan::FromMinutes(double minutes)
{
	return TimeSpan(minutes * MillisPerMinute);
}

TimeSpan TimeSpan::FromHours(double hours)
{
	return TimeSpan(hours * MillisPerHour);
}

TimeSpan TimeSpan::FromDays(double days)
{
	return TimeSpan(days * MillisPerDay);
}
