#pragma once
#include "Mesh.h"
#include <DirectXMath.h>

template <typename T>
class Model
{
protected:
	Mesh<T>* mesh;
	DirectX::XMVECTOR position;
	DirectX::XMVECTOR scale;
	DirectX::XMVECTOR rotation;
	XMMATRIX transformMatrix = XMMatrixIdentity();
public:
	Model(Mesh<T>* mesh)
	{
		this->mesh = mesh;
	}

	Mesh<T>* GetMesh() const
	{
		return this->mesh;
	}

	D3D10_PRIMITIVE_TOPOLOGY Topology = D3D10_PRIMITIVE_TOPOLOGY_UNDEFINED;

	void SetPosition(DirectX::XMVECTOR position)
	{
		this->position = position;

		transformMatrix = XMMatrixIdentity();
		transformMatrix = DirectX::XMMatrixMultiply(transformMatrix, DirectX::XMMatrixRotationRollPitchYawFromVector(rotation));
		transformMatrix = DirectX::XMMatrixMultiply(transformMatrix, DirectX::XMMatrixTranslationFromVector(position));
		transformMatrix = DirectX::XMMatrixMultiply(transformMatrix, DirectX::XMMatrixScalingFromVector(scale));

	}
	void SetRotation(DirectX::XMVECTOR rotation)
	{
		this->rotation = rotation;
		transformMatrix = DirectX::XMMatrixIdentity();
		transformMatrix = DirectX::XMMatrixMultiply(transformMatrix, DirectX::XMMatrixRotationRollPitchYawFromVector(rotation));
		transformMatrix = DirectX::XMMatrixMultiply(transformMatrix, DirectX::XMMatrixTranslationFromVector(position));
		transformMatrix = DirectX::XMMatrixMultiply(transformMatrix, DirectX::XMMatrixScalingFromVector(scale));
	}
	void SetScale(DirectX::XMVECTOR scale)
	{
		this->scale = scale;
		transformMatrix = XMMatrixIdentity();
		transformMatrix = DirectX::XMMatrixMultiply(transformMatrix, DirectX::XMMatrixRotationRollPitchYawFromVector(rotation));
		transformMatrix = DirectX::XMMatrixMultiply(transformMatrix, DirectX::XMMatrixTranslationFromVector(position));
		transformMatrix = DirectX::XMMatrixMultiply(transformMatrix, DirectX::XMMatrixScalingFromVector(scale));
	}

	DirectX::XMMATRIX GetMatrix()
	{
		return transformMatrix;
	}
};
