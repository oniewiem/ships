#pragma once
#include <vector>
#include "Logger.h"

template <typename T>
class Mesh
{
protected:
	std::vector<T> vertices;
	std::vector<UINT> indices;
public:
	Mesh()
	{
		if(std::is_same<T, VertexPositionColor>::value)
		{
			LOG_MESSAGE(1, "T is VertexPositionColor");
		}else if (std::is_same<T, VertexPositionNormal>::value)
		{
			LOG_MESSAGE(1, "T is VertexPositionNormal");
		}
		else if (std::is_same<T, VertexPositionNormalTexture>::value)
		{
			LOG_MESSAGE(1, "T is VertexPositionNormalTexture");
		}else
		{
			throw std::invalid_argument("Argument is not vertex type");
		}
	}
	const T* GetVerticesArray()			{ return vertices.data();}
	const UINT* GetIndicesArray()		{ return indices.data();}
	UINT GetVertexCount() const			{ return vertices.size();}
	UINT GetIndexCount() const			{ return indices.size();}
};
template <typename T>
class Axis : public Mesh<T>
{
public:
	Axis()
	{
		if (std::is_same<T, VertexPositionColor>::value)
		{
			vertices = std::vector<VertexPositionColor>() =
			{
				{ XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT4(1.0f,0.0f,0.0f,1.0f) },
				{ XMFLOAT3(1.0f, 0.0f, 0.0f), XMFLOAT4(1.0f,0.0f,0.0f,1.0f) },
				{ XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT4(0.0f,1.0f,0.0f,1.0f) },
				{ XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT4(0.0f,1.0f,0.0f,1.0f) },
				{ XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT4(0.0f,0.0f,1.0f,1.0f) },
				{ XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f,0.0f,1.0f,1.0f) }
			};
		}
		indices = std::vector<UINT>();
		for (UINT i = 0; i < vertices.size(); i += 2)
		{
			indices.push_back(i);
			indices.push_back(i + 1);
		}
	}
};


template <typename T>
class Cube : public Mesh<T>
{
public:
	Cube()
	{
		if (std::is_same<T, VertexPositionColor>::value)
		{
			vertices = std::vector<VertexPositionColor>() =
			{
				{ XMFLOAT3(-0.5f,  0.5f, -0.5f), XMFLOAT4(1.0f,1.0f,1.0f,1.0f) },
				{ XMFLOAT3( 0.5f,  0.5f, -0.5f), XMFLOAT4(0.5f,0.5f,0.5f,1.0f) },
				{ XMFLOAT3( 0.5f, -0.5f, -0.5f), XMFLOAT4(1.0f,1.0f,1.0f,1.0f) },
				{ XMFLOAT3(-0.5f, -0.5f, -0.5f), XMFLOAT4(0.5f,0.5f,0.5f,1.0f) },
				{ XMFLOAT3(-0.5f,  0.5f,  0.5f), XMFLOAT4(1.0f,1.0f,1.0f,1.0f) },
				{ XMFLOAT3( 0.5f,  0.5f,  0.5f), XMFLOAT4(0.5f,0.5f,0.5f,1.0f) },
				{ XMFLOAT3( 0.5f, -0.5f,  0.5f), XMFLOAT4(1.0f,1.0f,1.0f,1.0f) },
				{ XMFLOAT3(-0.5f, -0.5f,  0.5f), XMFLOAT4(0.5f,0.5f,0.5f,1.0f) }
			};
		}
		indices = std::vector<UINT>() = 
		{
			0,1,3,
			3,1,2,
			
			1,5,2,
			2,5,6,

			0,4,1,
			1,4,5,

			0,3,4,
			4,3,7,
			
			3,2,7,
			7,2,6,

			4,7,5,
			5,7,6

			
		};
		
	}
};