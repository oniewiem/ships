#pragma once
#include "TimeSpan.h"

class GameTime
{
public:
	GameTime(const TimeSpan&, const TimeSpan&);
	GameTime();
	~GameTime();

	TimeSpan GetTotalGameTime() const;
	TimeSpan GetElapsedGameTime() const;

	void SetTotalGameTime(TimeSpan &totalGameTime);
	void SetElapsedGameTime(TimeSpan &elapsedGameTime);
private: 
	TimeSpan totalGameTime;
	TimeSpan elapsedGameTime;
};

