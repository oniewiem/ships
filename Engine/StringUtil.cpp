#include "StringUtil.h"


std::string StringUtil::GetFormatedString(const char* format, va_list arg)
{
	const char* traverse;
	unsigned int i = 0;
	char *s;
	double d;

	std::stringstream output;

	for (traverse = format; *traverse != '\0'; traverse++)
	{
		while (*traverse != '%' && *traverse != '\0')
		{
			output << *traverse;
			traverse++;
		}
		traverse++;

		switch (*traverse)
		{
		case 'c':
			i = va_arg(arg, int);
			output << char(i);
			break;
		case 'd':
			i = va_arg(arg, int);
			if (i < 0)
			{
				i -= i;
				output << '-';
			}
			output << Convert(i, 10);
			break;

		case 'o':
			i = va_arg(arg, unsigned int);
			output << Convert(i, 8);
			break;
		case 's':
			s = va_arg(arg, char*);
			output << s;
			break;
		case 'x':
			i = va_arg(arg, unsigned int);
			output << Convert(i, 16);
			break;
		case 'f':
		case 'g':
			d = va_arg(arg, double);
			output << d;
		}
	}

	return output.str();
}

std::string StringUtil::GetFormatedString(const char* format, ...)
{
	va_list arg;
	va_start(arg, format);
	std::string output = GetFormatedString(format, arg);
	va_end(arg);
	return output;
}

char* StringUtil::Convert(unsigned int num, int base)
{
	static char Representation[] = "0123456789ABCDEF";
	static char buffer[50];
	char *ptr;

	ptr = &buffer[49];
	*ptr = '\0';

	do
	{
		*--ptr = Representation[num%base];
		num /= base;
	} while (num != 0);

	return(ptr);
}
