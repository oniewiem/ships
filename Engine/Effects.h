#pragma once
#include "DxUtil.h"
#include <fstream>
#include <vector>
#include <string>
#include "Light.h"

using DirectX::CXMMATRIX;
using DirectX::XMFLOAT3;

#pragma region Effect
class Effect
{
protected:
	ID3DX11Effect* fx;
public:
	Effect(ID3D11Device* device, const std::string& filename);
	virtual ~Effect();
private:
	/*Effect(const Effect& rhs);
	Effect& operator=(const Effect& rhs);*/
};
#pragma endregion

#pragma region BasicEffect
class BasicEffect : public Effect
{
public:
	BasicEffect(ID3D11Device* device, const std::string& filename);

	void SetWorldViewProjectionMatrix(CXMMATRIX m)				{ WorldViewProj->SetMatrix(reinterpret_cast<const float*>(&m)); }
	void SetWorldMatrix(CXMMATRIX m)							{ World->SetMatrix(reinterpret_cast<const float*>(&m)); }
	void SetWorldInvTransposeMatrix(CXMMATRIX m)				{ WorldInvTranspose->SetMatrix(reinterpret_cast<const float*>(&m)); }
	void SetEyePosition(const XMFLOAT3 v)						{ EyePosW->SetRawValue(&v, 0, sizeof(XMFLOAT3)); }
	void SetDirectionalLight(const DirectionalLight& light)		{ DirLight->SetRawValue(&light, 0, sizeof(DirectionalLight)); }
	void SetMaterial(const Material& material)					{ Mat->SetRawValue(&material, 0, sizeof(Material)); }

private:
	ID3DX11EffectTechnique* Light1Tech;
	ID3DX11EffectMatrixVariable* WorldViewProj;
	ID3DX11EffectMatrixVariable* World;
	ID3DX11EffectMatrixVariable* WorldInvTranspose;
	ID3DX11EffectVectorVariable* EyePosW;
	ID3DX11EffectVariable* DirLight;
	ID3DX11EffectVariable* Mat;
};
#pragma endregion
//
//#pragma region Effects
//class Effects
//{
//public:
//	static void AddEffect(Effect& effect);
//	static void RemoveAll();
//private:
//	static std::vector<Effect*> effects;
//};
//#pragma endregion