#pragma once


class TimeSpan
{

private:
	int milliseconds;

	static const int MillisPerSecond = 1000;
	static const int MillisPerMinute = MillisPerSecond * 60;
	static const int MillisPerHour = MillisPerMinute * 60;
	static const int MillisPerDay = MillisPerHour * 24;
	static const float SecondPerMillis;
	static const float MinutePerMillis;
	static const float HourPerMillis;
	static const float DayPerMillis;


public:
	TimeSpan() : TimeSpan(0)
	{
		
	};
	TimeSpan(unsigned int milliseconds);
	TimeSpan(unsigned int hours, unsigned int minutes, unsigned int seconds) : TimeSpan(0, hours, minutes, seconds, 0) {};

	TimeSpan(unsigned int days, unsigned int hours, unsigned int minutes, unsigned int seconds, unsigned int milliseconds) :
		TimeSpan(days * MillisPerDay + hours * MillisPerHour + minutes * MillisPerMinute + seconds * MillisPerSecond + milliseconds) {};
	~TimeSpan() {};


	static TimeSpan Zero();
	static TimeSpan FromSeconds(double seconds);
	static TimeSpan FromMinutes(double minutes);
	static TimeSpan FromHours(double hours);
	static TimeSpan FromDays(double days);

	inline short GetDays() const
	{
		return (milliseconds / MillisPerDay);
	}
	inline short GetHours() const
	{
		return (milliseconds / MillisPerHour) % 24;
	}
	inline short GetMinutes() const
	{
		return (milliseconds / MillisPerMinute) % 60;
	}
	inline short GetSeconds() const
	{
		return (milliseconds / MillisPerSecond) % 60;
	}
	inline short GetMilliseconds() const
	{
		return milliseconds % 1000;
	}

	inline double GetTotalDays() const
	{
		return milliseconds * DayPerMillis;
	}
	inline double GetTotalHours() const
	{
		return milliseconds * HourPerMillis;
	}
	inline double GetTotalMinutes() const
	{
		return milliseconds * MinutePerMillis;
	}
	inline double GetTotalSeconds() const
	{
		return  this->milliseconds * SecondPerMillis;
	}
	inline int GetTotalMilliseconds() const
	{
		return milliseconds;
	}

	inline bool operator<(const TimeSpan& ts) const
	{
		return milliseconds < ts.milliseconds;
	}
	inline bool operator>(const TimeSpan& ts) const
	{
		return milliseconds > ts.milliseconds;
	}
	inline bool operator<=(const TimeSpan& ts) const
	{
		return milliseconds <= ts.milliseconds;
	}
	inline bool operator>=(const TimeSpan& ts) const
	{
		return milliseconds >= ts.milliseconds;
	}
	inline bool operator==(const TimeSpan& ts) const
	{
		return milliseconds == ts.milliseconds;
	}
	inline bool operator!=(const TimeSpan& ts) const
	{
		return milliseconds != ts.milliseconds;
	}
	inline TimeSpan& operator+ (const TimeSpan& ts) const
	{
		return TimeSpan(milliseconds + ts.milliseconds);
	}
	inline TimeSpan& operator- (const TimeSpan& ts) const
	{
		return TimeSpan(milliseconds - ts.milliseconds);
	}

};

