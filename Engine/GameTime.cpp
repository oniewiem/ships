#include "GameTime.h"


GameTime::GameTime(const TimeSpan& totalGameTime, const TimeSpan& elapsedGameTime)
{
	this->totalGameTime = totalGameTime;
	this->elapsedGameTime = elapsedGameTime;
}

GameTime::GameTime() : GameTime(TimeSpan::Zero(), TimeSpan::Zero())
{

}

GameTime::~GameTime()
{
}

TimeSpan GameTime::GetTotalGameTime() const
{
	return totalGameTime;
}

TimeSpan GameTime::GetElapsedGameTime() const
{
	return elapsedGameTime;
}

void GameTime::SetTotalGameTime(TimeSpan &totalGameTime)
{
	this->totalGameTime = totalGameTime;
}

void GameTime::SetElapsedGameTime(TimeSpan &elapsedGameTime)
{
	this->elapsedGameTime = elapsedGameTime;
}
