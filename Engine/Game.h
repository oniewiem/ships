#pragma once
#define  WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <string>
#include "DxUtil.h"
#include "TimeSpan.h"
#include "GameTime.h"
#include "chrono"



class Game
{
public:
	Game(HINSTANCE hInstance);
	virtual ~Game();

	int Run();
	virtual bool Initialization();
	virtual void Update(const GameTime&) = 0;
	virtual void Draw(const GameTime&) = 0;
	virtual void OnResize();
	virtual LRESULT MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

private:
	unsigned int previousMilliseconds = 0;
	UINT MSAAx4Quality = 0;
	bool resizing = false;
	typedef std::chrono::high_resolution_clock TIMER;
	typedef std::chrono::milliseconds MILLISECONDS;
#define GET_MILLISECONDS std::chrono::duration_cast<MILLISECONDS>(TIMER::now().time_since_epoch()).count() 
	

	D3D_DRIVER_TYPE driverType;
	D3D_FEATURE_LEVEL featureLevel;
	D3D11_VIEWPORT viewport;


	TimeSpan accumulatedElapsedTime = TimeSpan::Zero();
	

	GameTime gameTime;
	bool InitWindow();
	bool InitDirecX();

	void CalculateFrameStats() const;
	void Tick();
protected:
	void SetTitle(std::string title);
protected:

	HWND hWnd;
	HINSTANCE hInstance;
	UINT width;
	UINT height;
	std::string title;
	DWORD style;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	ID3D11RenderTargetView* renderTargetView;
	IDXGISwapChain* swapChain;

	ID3D11Texture2D* depthStencilBuffer;
	ID3D11DepthStencilView* depthStencilView;
	TimeSpan targetElapsedTime = TimeSpan::FromSeconds(1.0 / 60);
public:
	bool enableMSAAx4 = false;

};

