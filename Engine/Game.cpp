#include "Game.h"
#include "../Game/WarShipGame.h"
#include <minwinbase.h>

namespace 
{
	Game *game = nullptr;
}

LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (game)
		return game->MsgProc(hwnd, msg, wParam, lParam);
	return DefWindowProc(hwnd, msg, wParam, lParam);
}

Game::Game(HINSTANCE hInstance) : depthStencilView(0), depthStencilBuffer(0)
{
	this->hInstance = hInstance;
	this->hWnd = nullptr;
	this->width = 800;
	this->height = 600;
	this->title = "DirectX11 App";
	this->style = WS_OVERLAPPEDWINDOW;
	game = this;


	this->device = nullptr;
	this->context = nullptr;
	this->swapChain = nullptr;
	this->renderTargetView = nullptr;
}


Game::~Game()
{
	if (context)
		context->ClearState();

	Memory::SaveRelease(renderTargetView);
	Memory::SaveRelease(depthStencilView);
	Memory::SaveRelease(swapChain);
	Memory::SaveRelease(depthStencilBuffer);
	Memory::SaveRelease(context);
	Memory::SaveRelease(device);

}

int Game::Run()
{
	MSG msg = { 0 };
	previousMilliseconds = GET_MILLISECONDS;
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			Tick();
		}
	}
	return  static_cast<int>(msg.wParam);
}

bool Game::Initialization()
{
	if(!InitWindow())
		return false;

	if(!InitDirecX())
		return false;
	return true;

}

void Game::OnResize()
{
	assert(context);
	assert(device);
	assert(swapChain);

	Memory::SaveRelease(renderTargetView);
	Memory::SaveRelease(depthStencilView);
	Memory::SaveRelease(depthStencilBuffer);

	HR(swapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0));
	ID3D11Texture2D* backBuffer;
	HR(swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer)));
	HR(device->CreateRenderTargetView(backBuffer, 0, &renderTargetView));
	Memory::SaveRelease(backBuffer);
	
	D3D11_TEXTURE2D_DESC depthStencilDesc;

	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

	depthStencilDesc.Width = width;
	depthStencilDesc.Height = height;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	if (enableMSAAx4)
	{
		depthStencilDesc.SampleDesc.Count = 4;
		depthStencilDesc.SampleDesc.Quality = MSAAx4Quality - 1;
	}
	else
	{
		depthStencilDesc.SampleDesc.Count = 1;
		depthStencilDesc.SampleDesc.Quality = 0;
	}
	HR(device->CreateTexture2D(&depthStencilDesc, nullptr, &depthStencilBuffer));
	HR(device->CreateDepthStencilView(depthStencilBuffer, nullptr, &depthStencilView));


	context->OMSetRenderTargets(1, &renderTargetView, depthStencilView);

	viewport.Width = static_cast<float>(width);
	viewport.Height = static_cast<float>(height);
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	UINT numberViewports = 1;
	context->RSSetViewports(1, &viewport);
}

LRESULT Game::MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		if(device)
		{
			if(wParam == SIZE_MAXIMIZED)
			{
				OnResize();
			}else if(wParam == SIZE_RESTORED)
			{
				if (!resizing)
					OnResize();
			}
		}
		return 0;
		break;

	case WM_ENTERSIZEMOVE:
		resizing = true;
		return 0;
		break;
	case WM_EXITSIZEMOVE:
		resizing = false;
		OnResize();
		return 0;
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	break;
	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
}

void Game::SetTitle(std::string title)
{
	this->title = title;
	SetWindowText(hWnd, title.c_str());
}

bool Game::InitWindow()
{
	WNDCLASSEX wcex;
	ZeroMemory(&wcex, sizeof(wcex));

	std::string className = "wndClassName";

	wcex.cbSize = sizeof(wcex);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.hInstance = this->hInstance;
	wcex.lpfnWndProc = MainWndProc;
	wcex.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	wcex.hIconSm = LoadIcon(nullptr, IDI_APPLICATION);
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = static_cast<HBRUSH>(GetStockObject(NULL_BRUSH));
	wcex.lpszClassName = className.c_str();

	if (!RegisterClassEx(&wcex))
	{
		LOG_ERROR(0, "Failed to register class");
		return false;
	}

	RECT rectangle = { 0, 0, width, height };
	AdjustWindowRect(&rectangle, style, FALSE);

	UINT windowsWidth = rectangle.right - rectangle.left;
	UINT windowsHeight = rectangle.bottom - rectangle.top;
	UINT x = 0, y = 0;

	hWnd = CreateWindow(className.c_str(), title.c_str(), style, x, y, windowsWidth, windowsHeight, nullptr, nullptr, hInstance, nullptr);


	if(!hWnd)
	{
		LOG_ERROR(0, "Failed to create window");
		return false;
	}

	ShowWindow(hWnd, SW_SHOW);

	return true;
}

bool Game::InitDirecX()
{
	UINT createDeviceFlags = D3D11_CREATE_DEVICE_SINGLETHREADED;
	#if defined(DEBUG) || defined(_DEBUG)  
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
	#endif



	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE
	};

	UINT numberDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[]
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3
	};
	UINT numberFeatureLevels = ARRAYSIZE(featureLevels);

	HRESULT result;
	for (int i = 0; i < numberDriverTypes; ++i)
	{
		result = D3D11CreateDevice(nullptr, driverTypes[i], nullptr, createDeviceFlags, featureLevels, numberFeatureLevels, D3D11_SDK_VERSION, &device, &featureLevel, &context);

		if (SUCCEEDED(result))
		{
			driverType = driverTypes[i];
			break;
		}
	}
	if (FAILED(result))
	{
		LOG_ERROR(0, "Failed to create device");
		return false;
	}



	HR(device->CheckMultisampleQualityLevels(
		DXGI_FORMAT_R8G8B8A8_UNORM, 4, &MSAAx4Quality));
	assert(MSAAx4Quality > 0);

	DXGI_SWAP_CHAIN_DESC swapDesc;


	ZeroMemory(&swapDesc, sizeof(swapDesc));

	swapDesc.BufferCount = 1; // double buffered
	swapDesc.BufferDesc.Width = width;
	swapDesc.BufferDesc.Height = height;
	swapDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapDesc.OutputWindow = hWnd;
	swapDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapDesc.Windowed = true;
	swapDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	if (enableMSAAx4)
	{
		swapDesc.SampleDesc.Count = 4;
		swapDesc.SampleDesc.Quality = MSAAx4Quality - 1;
	}
	else
	{
		swapDesc.SampleDesc.Count = 1;
		swapDesc.SampleDesc.Quality = 0;
	}




	IDXGIDevice* dxgiDevice = nullptr;
	HR(device->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&dxgiDevice)));

	IDXGIAdapter* dxgiAdapter = nullptr;
	HR(dxgiDevice->GetParent(__uuidof(IDXGIAdapter), reinterpret_cast<void**>(&dxgiAdapter)));

	IDXGIFactory* dxgiFactory = nullptr;
	HR(dxgiAdapter->GetParent(__uuidof(IDXGIFactory), reinterpret_cast<void**>(&dxgiFactory)));

	HR(dxgiFactory->CreateSwapChain(device, &swapDesc, &swapChain));

	Memory::SaveRelease(dxgiDevice);
	Memory::SaveRelease(dxgiAdapter);
	Memory::SaveRelease(dxgiFactory);


		OnResize();

		return true;
	}

	void Game::CalculateFrameStats() const
	{
		static int frameCnt = 0;
		static float timeElapsed = 0;

		frameCnt++;

		auto time = gameTime.GetTotalGameTime().GetTotalSeconds();
		if((time - timeElapsed) >= 1.0f)
		{
			float fps = static_cast<float>(frameCnt);
			float mspf = 1000.0f / fps;

			SetWindowText(hWnd, StringUtil::GetFormatedString("%s   FPS:%f    Frame Time:%f ms", title.c_str(), fps, mspf).c_str());

			frameCnt = 0;
			timeElapsed += 1.0f;
		}

	}

void Game::Tick()
{
	while (true)
	{
		unsigned int milliseconds = GET_MILLISECONDS;
		accumulatedElapsedTime = TimeSpan(milliseconds - previousMilliseconds) + accumulatedElapsedTime;
		previousMilliseconds = milliseconds;
		if (accumulatedElapsedTime < targetElapsedTime)
			_sleep((targetElapsedTime - accumulatedElapsedTime).GetTotalMilliseconds());
		else
			break;
	}


	CalculateFrameStats();
	gameTime.SetElapsedGameTime(targetElapsedTime);
	while(accumulatedElapsedTime >= targetElapsedTime)
	{
		gameTime.SetTotalGameTime(gameTime.GetTotalGameTime() + targetElapsedTime);
		accumulatedElapsedTime = accumulatedElapsedTime -targetElapsedTime;
		Update(gameTime);
	}

	Draw(gameTime);

}
