#pragma once
#include <sstream>
#include <string>
#include <cstdarg>
class StringUtil
{
public:
	static std::string GetFormatedString(const char*, va_list);
	static std::string GetFormatedString(const char*, ...);

	static char* Convert(unsigned int, int);
};

