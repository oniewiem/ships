#pragma once
#include <DirectXMath.h>
#include <vector>

using namespace DirectX;
struct VertexPositionColor
{
	XMFLOAT3 Position;
	XMFLOAT4 Color;
};

struct VertexPositionNormal
{
	XMFLOAT3 Position;
	XMFLOAT3 Normal;
};

struct VertexPositionNormalTexture
{
	XMFLOAT3 Position;
	XMFLOAT3 Normal;
	XMFLOAT2 Tex;
};


const D3D11_INPUT_ELEMENT_DESC VertexPositionColorDesc[2] = 
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "COLOR",    0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};


const D3D11_INPUT_ELEMENT_DESC VertexPositionNormalDesc[2] = 
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};



const D3D11_INPUT_ELEMENT_DESC VertexPositionNormalTextureDesc[3] = 
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};
