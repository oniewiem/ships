#include "Logger.h"
#include <ctime>


static Logger* instance;

Logger* Logger::GetInstance()
{
	if (!instance)
		instance = new Logger;
	return instance;
}


void Logger::Log(const char* file, int line, const char* functionName, const char* kind, int logLevel, const char* format, ...)
{
	va_list arg;
	va_start(arg, format);
	std::string text  = StringUtil::GetFormatedString(format, arg);
	va_end(arg);
	OutputDebugString(ComposeLogToString(file, line, functionName, kind, logLevel, text.c_str()).c_str());
}

std::string Logger::ComposeLogToString(const char* file, int line, const char* functionName, const char* kind, int logLevel, const char* text)
{
	std::stringstream output;
	time_t now = time(0);
	struct tm tStruct;
	char buf[80];
	tStruct = *localtime(&now);
	strftime(buf, sizeof(buf), "%X", &tStruct);

	output << "[" << buf << "]";
	output << "[" << kind << "]";
	output << "[" << logLevel << "]";
	output << "[" << file << "]";
	output << "[" << functionName << "]";
	output << "[" << line << "]";
	output << text << std::endl;

	return output.str();
}

